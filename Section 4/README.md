# Section 4

Цель данной секции - понять принцип работы REST API сервера 

Для выполнения задания вам потребуются следующие навыки:
 - Знания языка ECMAScript (JavaScript)
 - Навыки работы с пакетными менеджерами (npm, yarn)
 - Опыт использования веб сервера Express
 
##### 1. Настройка NodeJS сервера

Для начала необходимо установить все зависимости из package.json

```
npm install
```

Главный исполняемый файл будущего веб сервера index.js

Изначально в нём содержится исходный код простого сервера который вы должны адаптировать

Запуск сервера:

```
node index.js
```

Сервер запускается на локальной машине слушая порт 3000

Проверить работоспособность можно по адресу 127.0.0.1:3000


##### 2. REST API сервер

Создайте endpoint-ы:
 - /auth/login
 - /auth/registration
 - /market/categories
 - /market/categories/$category_id/goods
 - /market/categories/$category_id/goods/$product_id/
 - /market/basket/add
 - /market/basket/remove
 - /market/basket/checkout 

Каждый метод должен возвращать результат в json формате.

##### 3. Подключение клиента

Создайте реализацию интерфейса IDataProvider - RESTDataProvider

Подключите серверную авторизацию и регистрацию 